using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSit : MonoBehaviour
{
    public Animator PlayerAniMator;

    [SerializeField] bool IsActive;

   [SerializeField] bool IsSitting = false ; 


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Chair")
        {
            IsActive = true;
            other.transform.GetChild(0).gameObject.SetActive(true);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Chair")
        {
            IsActive = false;
            other.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {


        if (other.tag == "Chair" && IsActive)
        {
           
            if (Input.GetKeyDown(KeyCode.E) && IsSitting == false ) // Seated
            {
                Debug.Log("Sit");
                transform.GetComponent<Rigidbody>().isKinematic = true;
                transform.GetComponent<Rigidbody>().useGravity = false;
                




                transform.position =  other.transform.GetChild(1).transform.position;
                transform.rotation =  other.transform.GetChild(1).transform.rotation;
                
                PlayerAniMator.SetBool("Sit", true);
                IsSitting = true;
                transform.GetComponent<SUPERCharacter.SUPERCharacterAIO>().enableMovementControl = false;
            }else if (Input.GetKeyDown(KeyCode.E) && IsSitting == true ) // Seated
            {
                Debug.Log("Up");
                transform.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetComponent<Rigidbody>().useGravity = true;


                transform.position = other.transform.GetChild(2).transform.position;
                

                PlayerAniMator.SetBool("Sit", false);
                IsSitting = false;
                transform.GetComponent<SUPERCharacter.SUPERCharacterAIO>().enableMovementControl = enabled;
            }
        }
    }
}
