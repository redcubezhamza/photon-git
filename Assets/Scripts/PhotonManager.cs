using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;


public class playerName
{
    [SerializeField]
    public string username;
    [SerializeField]
    public playerName()
    {
      
    }
    [SerializeField]
    public  playerName(string u)
    {
        this.username = u;
    }
}
public class PhotonManager : MonoBehaviourPunCallbacks
{
    public InputField username;
    public InputField RoomName;
    [SerializeField]
    public static playerName playername = new playerName();
    public GameObject loginPanel, loading, lobbyPanel,createRoom, RoomListPanel, Game,playBtn;
   

    public GameObject prefabItem,prefabParent, prefabItemInsideRoom,prefabItemInsideRoomParent;
    private Dictionary<string, RoomInfo> roomListData;
    private Dictionary<string, GameObject> roomListGameObject;
    private Dictionary<int, GameObject> playerListGameObject;

    [Header("inside the room")] public GameObject insideRoomPanel;
    #region unity methods
    // Start is called before the first frame update
    void Start()
    {
        handlePanels(loginPanel.name);
        roomListData = new Dictionary<string, RoomInfo>();
        roomListGameObject = new Dictionary<string, GameObject>();
        PhotonNetwork.AutomaticallySyncScene = true;
        
        
    }
  
    // Update is called once per frame
    void Update()
    {
        Debug.Log("NetworkState " + PhotonNetwork.NetworkClientState);
    }
   
    public void Login()
    {
        string defaultname = string.Empty;
        string name = username.text;
        PlayerPrefs.SetString("name", name);
        PhotonNetwork.NickName = defaultname;
        if (!string.IsNullOrEmpty(name))
        {
            PhotonNetwork.LocalPlayer.NickName = name;
            PhotonNetwork.ConnectUsingSettings();
            handlePanels(loading.name);
        }
        else
        {
            Debug.Log("Please Enter Name");
        }
    }

    public void createRoomButton()
    {
        string roomName = RoomName.text;
        if (string.IsNullOrEmpty(roomName))
        {
            roomName = roomName + Random.Range(0, 1000);
        }
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)14;
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    public void cancel()
    {

        handlePanels(lobbyPanel.name);
    }
    public void RoolList()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
        handlePanels(RoomListPanel.name);
    }
    public void BackRoolList()
    {
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
        handlePanels(lobbyPanel.name);
    }
    public void BackplayerList()
    {
        if (PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();
        }
        handlePanels(lobbyPanel.name);
    }
    #endregion

    #region photon callbacks

    public override void OnConnected()
    {
        Debug.Log("connect to the internet");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " Connected to the photon");
        handlePanels(lobbyPanel.name);

      
    }
    

    public override void OnCreatedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name + " is created");

        Debug.Log("region code is -->  "+PhotonNetwork.CloudRegion);
    }
    public override void OnJoinedRoom()
    {

        playername.username = PhotonNetwork.LocalPlayer.NickName;
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " Joined the room");
        handlePanels(insideRoomPanel.name);

        if(playerListGameObject == null)
        {
            playerListGameObject = new Dictionary<int, GameObject>();
        }


        if (PhotonNetwork.IsMasterClient)
        {
            playBtn.SetActive(true);
        }
        else
        {
            playBtn.SetActive(false);
        }


        foreach(Player p in PhotonNetwork.PlayerList)
        {
            GameObject playerList = Instantiate(prefabItemInsideRoom);
            playerList.transform.SetParent(prefabItemInsideRoomParent.transform);
            playerList.transform.GetChild(0).transform.GetComponent<Text>().text = p.NickName;
            if(p.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
            {
                playerList.transform.GetChild(1).transform.gameObject.SetActive(true);
                playerList.transform.GetChild(1).transform.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "You";
            }
            else
            {
                playerList.transform.GetChild(1).transform.gameObject.SetActive(false);
            }

            playerListGameObject.Add(p.ActorNumber, playerList);
        }
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        playername.username = PhotonNetwork.LocalPlayer.NickName;
        GameObject playerList = Instantiate(prefabItemInsideRoom);
        playerList.transform.SetParent(prefabItemInsideRoomParent.transform);
        playerList.transform.GetChild(0).transform.GetComponent<Text>().text = newPlayer.NickName;
        if (newPlayer.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            playerList.transform.GetChild(1).transform.gameObject.SetActive(true);
            playerList.transform.GetChild(1).transform.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "You";
        }
        else
        {
            playerList.transform.GetChild(1).transform.gameObject.SetActive(false);
        }

        playerListGameObject.Add(newPlayer.ActorNumber, playerList);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(playerListGameObject[otherPlayer.ActorNumber]);
        playerListGameObject.Remove(otherPlayer.ActorNumber);

        if (PhotonNetwork.IsMasterClient)
        {
            playBtn.SetActive(true);
        }
        else
        {
            playBtn.SetActive(false);
        }
    }

    public void playBtnMethod()
    {
        playername.username = PhotonNetwork.LocalPlayer.NickName;
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("PMC");
        }
       
    }
    public override void OnLeftRoom()
    {
       
        handlePanels(lobbyPanel.name);
        foreach(GameObject obj in playerListGameObject.Values)
        {
            Destroy(obj);
        }
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        //clearList();
        playername.username = PhotonNetwork.LocalPlayer.NickName;
        foreach (RoomInfo rooms in roomList)
        {

            Debug.Log("Room Name " + rooms.Name + " Max Player" + rooms.MaxPlayers + " Joined Players " + rooms.PlayerCount);
            if(!rooms.IsOpen || !rooms.IsVisible || rooms.RemovedFromList)
            {
                if (roomListData.ContainsKey(rooms.Name))
                {
                    roomListData.Remove(rooms.Name);
                }
            }
            else
            {
                if (roomListData.ContainsKey(rooms.Name))
                {
                    roomListData[rooms.Name] = rooms;
                }
                else
                {
                    roomListData.Add(rooms.Name, rooms);
                }
            }
        
        }
        foreach (RoomInfo roomsItem in roomListData.Values)
        {
            GameObject item = Instantiate(prefabItem);
            item.transform.SetParent(prefabParent.transform);
            item.transform.GetChild(0).transform.GetComponent<Text>().text = roomsItem.Name;
            item.transform.GetChild(1).transform.GetComponent<Text>().text = roomsItem.MaxPlayers.ToString();
            item.transform.GetChild(2).transform.GetComponent<Text>().text = roomsItem.PlayerCount.ToString();
            item.transform.GetChild(3).transform.GetComponent<Button>().onClick.AddListener(() => joinRoomFromList(roomsItem.Name));
            roomListGameObject.Add(roomsItem.Name, item);
            Debug.Log("Room Name "+ roomsItem.Name);
        }
    }
    public override void OnLeftLobby()
    {
        clearList();
        roomListData.Clear();
    }
    public void joinRoomFromList(string roomName)
    {
       
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
        PhotonNetwork.JoinRoom(roomName);
    }
    public void clearList()
    {
        if(roomListGameObject.Count > 0)
        {
            foreach (var v in roomListGameObject.Values)
            {
                Destroy(v);
            }
            roomListGameObject.Clear();
        }
        
    }
    #endregion

    #region public_method

    public void handlePanels(string panelName)
    {
        lobbyPanel.SetActive(panelName.Equals(lobbyPanel.name));
        loading.SetActive(panelName.Equals(loading.name));
        createRoom.SetActive(panelName.Equals(createRoom.name));
        RoomListPanel.SetActive(panelName.Equals(RoomListPanel.name));
        insideRoomPanel.SetActive(panelName.Equals(insideRoomPanel.name));
        // Game.SetActive(panelName.Equals(Game.name));
    }

    #endregion
}
