using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class gameManager : MonoBehaviourPunCallbacks
{
    public static gameManager instance;

    public Camera playerCam;
    [SerializeField] GameObject player;
    [SerializeField] Transform[] pos;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            int random = Random.Range(0, pos.Length);
            GameObject playerMine =  PhotonNetwork.Instantiate(player.name, new Vector3(pos[random].position.x, 0f, pos[random].position.z), Quaternion.identity);

            playerCam = playerMine.GetComponentInChildren<Camera>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
