using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChairsArrow : MonoBehaviour
{
    Vector3 meanPosition;
    Vector3 upPos, downPos;

    Vector3 targetPos;


    [SerializeField] Canvas canvas;
    [SerializeField] Camera camForCanvas;

    private float sitChairSpeed;
    

    // Start is called before the first frame update
    void Start()
    {
        meanPosition = transform.position;
        upPos = new Vector3 (transform.position.x, meanPosition.y + 0.1f, meanPosition.z);
        downPos = new Vector3(transform.position.x, meanPosition.y - 0.1f, meanPosition.z);

        targetPos = upPos;

        canvas.gameObject.SetActive(false);

        //if (transform.Find("Camera").gameObject == isActiveAndEnabled)
        //{
        //    camForCanvas = transform.Find("Camera").GetComponent<Camera>();

        //    canvas.worldCamera = camForCanvas;
        //}


        //  camForCanvas = GameObject.FindGameObjectWithTag("playeruser").GetComponentInChildren<Camera>();


     //   camForCanvas = gameManager.instance.playerCam;

        if (camForCanvas != null)
        {
            Debug.Log("<color=green>Found Player Camera for Chairs Canvas</color>");

            canvas.worldCamera = camForCanvas;
        }

        sitChairSpeed = 0.5f;



    }

    // Update is called once per frame
    void Update()
    {
        // transform.Rotate(1, 0, 0, Space.Self);

        

        transform.position = Vector3.MoveTowards(transform.position, targetPos, sitChairSpeed  * Time.deltaTime);

        if (transform.position == upPos)
        {
            targetPos = downPos;
        }

        if (transform.position == downPos)
        {
            targetPos = upPos;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            canvas.gameObject.SetActive(false);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("playeruser"))
        {
            canvas.gameObject.SetActive(true);
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("playeruser"))
        {
            canvas.gameObject.SetActive(false);
        }

    }
}
