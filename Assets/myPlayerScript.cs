
using UnityEngine;

using Photon.Pun;
public class myPlayerScript : MonoBehaviourPun
{
    PhotonView _pv;
   
    private void Awake()
    {
        _pv = gameObject.GetComponent<PhotonView>(); 
    }
   
  

    
    

    private void OnTriggerStay(Collider other)    // if player stays then enable camera and allow player to write on board
    {
      
        if (other.gameObject.CompareTag("BoardLogic"))
        {//

            if (_pv.IsMine)
            {
                if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);
                }

            }
        }
        else if (other.gameObject.CompareTag("BoardLogic1"))
        {//

            if (_pv.IsMine)
            {
                if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);
                }

            }
        }
        else if (other.gameObject.CompareTag("BoardLogic2"))
        {//

            if (_pv.IsMine)
            {
                if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);
                }

            }
        }


    }



    private void OnTriggerExit(Collider other)
    {
    
        if (other.gameObject.CompareTag("BoardLogic"))  // detect collision nd proseed as per board 
        {
            if (_pv.IsMine)
            {
                if (other.gameObject.GetComponent<draw>().isCollided && other.gameObject.GetComponent<draw>().boardButton.gameObject.activeSelf)
                {
                   
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);
                }
            }
        }
        else if (other.gameObject.CompareTag("BoardLogic1"))
        {
            if (_pv.IsMine)
            {
                if (other.gameObject.GetComponent<draw>().isCollided && other.gameObject.GetComponent<draw>().boardButton.gameObject.activeSelf)
                {
                    //      _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);
                }
            }
        }
        else if (other.gameObject.CompareTag("BoardLogic2"))
        {
            if (_pv.IsMine)
            {
                if (other.gameObject.GetComponent<draw>().isCollided && other.gameObject.GetComponent<draw>().boardButton.gameObject.activeSelf)
                {
                   
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);
                }
            }
        }


    }

    [PunRPC]
    void SetLineRenderBoardTrue(string tagval)
    {
   
        GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().isCollided=true;
        
    }

    [PunRPC]
    void SetLineRenderBoardFalse(string tagval)
    {
        GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().isCollided = false;
    }
}


