using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Painting : MonoBehaviour
{
    public string url;
    Texture2D YourRawImage;

    void Start()
    {
        StartCoroutine(DownloadImage(url));
         
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        YourRawImage = ((DownloadHandlerTexture)request.downloadHandler).texture;
        transform.gameObject.GetComponent<MeshRenderer>().material.mainTexture = YourRawImage;
    }

}
