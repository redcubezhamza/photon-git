using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtObject : MonoBehaviour
{
    [SerializeField] GameObject _object;
    // Start is called before the first frame update
    void Start()
    {
        //  _object = GameObject.FindGameObjectWithTag("/playeruser").GetComponentInChildren<Camera>().gameObject;

        // _object = _object = GameObject.Find("/Character.001/Camera");



        StartCoroutine(ExecuteAfterTime(5));
        //_object = gameManager.instance.playerCam.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (_object != null)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - _object.transform.position);
        }
        
    }


    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        _object = gameManager.instance.playerCam.gameObject;

    }
}
