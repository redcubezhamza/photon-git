using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System.Collections.Generic;
using System.Collections;
using System;

public class draw : MonoBehaviourPun 
{
    public  PhotonView _Photonview;
    public Camera myCamera;
    public Camera TempCameraBoard;
    public Button boardButton,deleteAllBtn;
    public bool IsOccupied=true;
    public bool isCollided;
    public float OffsetBoard;
    public bool BoardForTeacher;
    public string BoardName;
    // Sticky button Logic
    public Button StickyBoardbutton;
    public bool stickyBool;
    public Button StickyButton_WriteOnNote, StickyButton_MoveBoard, StickyButton_EraseNote, stickyButton_TypeOnNote;
    // Start is called before the first frame update
    void Start()
    {
      
       _Photonview= gameObject.GetComponent<PhotonView>();
        boardButton.onClick.AddListener(SetCameraAction);   // Listner for Button 
        deleteAllBtn.onClick.AddListener(DeleteAllTrails); // Erase button 
        StickyBoardbutton.onClick.AddListener(CheckforStickyButton); // Sticky board Button 

        StickyButton_WriteOnNote.onClick.AddListener(WriteOnNoteCreated);
        StickyButton_MoveBoard.onClick.AddListener(MoveTheNoteSelected);
        StickyButton_EraseNote.onClick.AddListener(EraseNoteSelected);
        stickyButton_TypeOnNote.onClick.AddListener(TypeOnNoteSelected);

    }
    [HideInInspector]
    public Transform marker;   // get the refrence for marker transform 
    bool chk; //bool for local use 

    bool typeOnNote;
    bool WriteNoteBool;
    bool MoveSelectedNote;
    bool EraseNoteSelectedbool;

    void TypeOnNoteSelected()
    {
        typeOnNote = true;
        WriteNoteBool = false;
        MoveSelectedNote = false;
        EraseNoteSelectedbool = false;
    }
    void WriteOnNoteCreated()
    {
        WriteNoteBool = true;
        typeOnNote = false;
        MoveSelectedNote = false;
        EraseNoteSelectedbool = false;
    }
    void MoveTheNoteSelected()
    {
        MoveSelectedNote = true;
        typeOnNote = false;
        WriteNoteBool = false;
        EraseNoteSelectedbool = false;
    }
    void EraseNoteSelected()
    {
        EraseNoteSelectedbool = true;
        typeOnNote = false;
        MoveSelectedNote = false;
        WriteNoteBool = false;
        chk4 = false;
    }
    void CheckforStickyButton()
    {
        stickyBool = true; // = !stickyBool;
        chk2 = true;

    }
    void DeleteAllTrails()
    {
        _Photonview.RPC("DeleteAllMarkers", RpcTarget.All);   // send command to other players to remove last created draw
    }
   
  [PunRPC]
    public void DeleteAllMarkers()
    {
        if (ob.Count > 1)
        {
            GameObject nextObect = ob.Pop();
            Destroy(nextObect);
            if (ob.Count != 0)
            {
                nextObect = ob.Pop();    // pushing and pop from stack . as each trail is saved in stack
                Destroy(nextObect);
            }
        }
    }

    void SetCameraAction()
    {
        chk = !chk;
        CallBoardLogic(chk);
 
    }
    
   public void CallBoardLogic(bool val)
    {
        if (val)
        {
            myCamera.gameObject.SetActive(val);
        }
        else
        {
            myCamera.gameObject.SetActive(val);
        }
    }

    bool chk1 = true; // spawn marker only once
    bool chk2 = false; // spawn sticky check box
    bool chk3 = true; //Check for Sticky note marker
    bool chk4 = false; // Check to call the function once;to remove data
   public  Transform temptransform_localUse; // local refrence 
  public  Transform tempTeansformForNoteMarker;
  public  Transform Temp_StickyNote_Refrence;
    public static Stack<GameObject> ob = new Stack<GameObject>();
    

    private void Update()
    {
        if (boardButton.gameObject.activeSelf)
        {
            if (!myCamera.gameObject.activeSelf)
            {
                boardButton.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Enter Board";
                StickyBoardbutton.gameObject.SetActive(false);  // disable sticky button
                StickyButton_WriteOnNote.gameObject.SetActive(false);
                StickyButton_MoveBoard.gameObject.SetActive(false);
                StickyButton_EraseNote.gameObject.SetActive(false);
                stickyButton_TypeOnNote.gameObject.SetActive(false);
                EraseNoteSelectedbool = false;
                MoveSelectedNote = false;
                WriteNoteBool = false;
            }
            else
            {
                boardButton.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Exit Board";
                StickyBoardbutton.gameObject.SetActive(true); // Enable sticky button
                if (BoardName == "Marker")
                {
                    if (GameObject.FindGameObjectsWithTag("StickyNote") != null)
                        if (GameObject.FindGameObjectsWithTag("StickyNote").Length >= 1)
                        {
                            StickyButton_WriteOnNote.gameObject.SetActive(true);
                            StickyButton_MoveBoard.gameObject.SetActive(true);
                            StickyButton_EraseNote.gameObject.SetActive(true);
                            stickyButton_TypeOnNote.gameObject.SetActive(true) ;
                        }
                }
                else if (BoardName == "Marker1")
                {
                    if (GameObject.FindGameObjectsWithTag("StickyNote1") != null)
                        if (GameObject.FindGameObjectsWithTag("StickyNote1").Length >= 1)
                        {
                            StickyButton_WriteOnNote.gameObject.SetActive(true);
                            StickyButton_MoveBoard.gameObject.SetActive(true);
                            StickyButton_EraseNote.gameObject.SetActive(true);
                            stickyButton_TypeOnNote.gameObject.SetActive(true);
                        }
                }
                else if (BoardName == "Marker2")
                {
                    if (GameObject.FindGameObjectsWithTag("StickyNote2") != null)
                        if (GameObject.FindGameObjectsWithTag("StickyNote2").Length >= 1)
                        {
                            StickyButton_WriteOnNote.gameObject.SetActive(true);
                            StickyButton_MoveBoard.gameObject.SetActive(true);
                            StickyButton_EraseNote.gameObject.SetActive(true);
                            stickyButton_TypeOnNote.gameObject.SetActive(true);
                        }
                }
            }
        }
    }

    [PunRPC]
    void DeleteSelectedNoteBoard(int photonid)
    {

        if (PhotonView.Find(photonid).IsMine)
        {
            if (PhotonView.Find(photonid).gameObject.name == "StickyNote")
            {
                GameObject[] temp = GameObject.FindGameObjectsWithTag("MarkerForNote");
                foreach (GameObject b in temp)
                   PhotonNetwork.Destroy(b);
            }
            else if (PhotonView.Find(photonid).gameObject.name == "StickyNote1")
            {
                GameObject[] temp = GameObject.FindGameObjectsWithTag("MarkerForNote1");
                foreach (GameObject b in temp)
                    PhotonNetwork.Destroy(b);
            }
            else if (PhotonView.Find(photonid).gameObject.name == "StickyNote2")
            {
                GameObject[] temp = GameObject.FindGameObjectsWithTag("MarkerForNote2");
                foreach (GameObject b in temp)
                    PhotonNetwork.Destroy(b);
            }

            PhotonNetwork.Destroy(PhotonView.Find(photonid));

           // print(GameObject.FindGameObjectsWithTag("StickyNote").Length);
        }
        print(GameObject.FindGameObjectsWithTag("StickyNote").Length ) ;
    }

    [PunRPC]
    void BroadCastNamesOfSpawnedNotes(int photonid,string name)
    {

      //  if (PhotonView.Find(photonid).IsMine)
        
            PhotonView.Find(photonid).gameObject.name=name;
   

    }
    IEnumerator  GetALL_DataToRemove(GameObject Data)
    {
        print("Chk111");
        if (Data.transform.childCount > 0)
            for (int i = 0; i < Data.transform.childCount; i++)
                if (Data.transform.GetChild(i).GetComponent<PhotonView>().Owner.ActorNumber != PhotonNetwork.LocalPlayer.ActorNumber)
                    Data.transform.GetChild(i).GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);
      
      
        

        yield return new WaitForSeconds(1.5f);  // Delay as to transfr owner ship
        if (Data.transform.childCount > 0)
            for (int i = 0; i < Data.transform.childCount; i++)
                if (Data.transform.GetChild(i).GetComponent<PhotonView>().Owner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
                    PhotonNetwork.Destroy(Data.transform.GetChild(i).gameObject);
  //      Data.transform.GetChild(i).GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);

        PhotonNetwork.Destroy(Data);
        print("Chk11");
        //  PhotonNetwork.Destroy(Data);

        if (BoardName == "Marker")
        {     //   print(GameObject.FindGameObjectsWithTag("StickyNote").Length);
            if (GameObject.FindGameObjectsWithTag("StickyNote") != null)
                if (GameObject.FindGameObjectsWithTag("StickyNote").Length <= 1)
                {
                  //  print(GameObject.FindGameObjectsWithTag("StickyNote").Length);
                    // Debug.LogError("dfjgbd");
                    StickyButton_WriteOnNote.gameObject.SetActive(false);
                    StickyButton_MoveBoard.gameObject.SetActive(false);
                    StickyButton_EraseNote.gameObject.SetActive(false);
                    stickyButton_TypeOnNote.gameObject.SetActive(false);
                }
        }
        else if (BoardName == "Marker1")
        {      //   print(GameObject.FindGameObjectsWithTag("StickyNote").Length);
            if (GameObject.FindGameObjectsWithTag("StickyNote1") != null)
                if (GameObject.FindGameObjectsWithTag("StickyNote1").Length <= 1)
                {
                    // Debug.LogError("dfjgbd");
                    StickyButton_WriteOnNote.gameObject.SetActive(false);
                    StickyButton_MoveBoard.gameObject.SetActive(false);
                    StickyButton_EraseNote.gameObject.SetActive(false);
                    stickyButton_TypeOnNote.gameObject.SetActive(false);
                }
        }
        else if (BoardName == "Marker2")
        {      //   print(GameObject.FindGameObjectsWithTag("StickyNote").Length);
            if (GameObject.FindGameObjectsWithTag("StickyNote2") != null)
                if (GameObject.FindGameObjectsWithTag("StickyNote2").Length <= 1)
                {
                    // Debug.LogError("dfjgbd");
                    StickyButton_WriteOnNote.gameObject.SetActive(false);
                    StickyButton_MoveBoard.gameObject.SetActive(false);
                    StickyButton_EraseNote.gameObject.SetActive(false);
                    stickyButton_TypeOnNote.gameObject.SetActive(false);
                }
        }
        yield return null;
     //   _Photonview.RPC("DeleteSelectedNoteBoard", RpcTarget.All, data.GetComponent<PhotonView>().ViewID);

    }
    //GameObject temprefrence;
    [PunRPC]
    void DeleteAllNoteParentAndChild()
    {
       // Destroy(temprefrence);
    }


   // public GameObject[] GetTrailRef;
    bool chk5; // check for trail once 
    void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && myCamera.gameObject.activeSelf)   // its  local me so draw to board
        {
            Ray ray;
            RaycastHit hit;
            ray = myCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {

                if ((hit.collider.name == "StickyNote" || hit.collider.name == "StickyNote1" || hit.collider.name == "StickyNote2") && MoveSelectedNote)
                {
                    if(Temp_StickyNote_Refrence == null) // Other client is accessing data
                    {
                        Temp_StickyNote_Refrence = hit.collider.gameObject.transform;
                    }
       //             if (Temp_StickyNote_Refrence.childCount == 0)  // do tempTeansformForNoteMarker // donot move if 
         //           {
                        if (Temp_StickyNote_Refrence != hit.collider.gameObject.transform)
                            Temp_StickyNote_Refrence = hit.collider.gameObject.transform;


                        if (hit.collider.gameObject.GetComponent<PhotonView>().Owner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
                        {
                        print("Owned Bro");
                           // StartCoroutine(GetALL_DataToRemove(hit.collider.gameObject));
                        }
                        else
                        {
                        print("Not Owned Working on it");
                        hit.collider.gameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);
                       
                    }



                        //SetParentPosition for each board

                        if (BoardName == "Marker")
                            hit.collider.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y, OffsetBoard);

                        else if (BoardName == "Marker1")
                            hit.collider.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y, OffsetBoard);

                        else if (BoardName == "Marker2")
                            hit.collider.gameObject.transform.position = new Vector3(OffsetBoard, hit.point.y, hit.point.z);


           //         }
                    

                }
                if ((hit.collider.name == "StickyNote" || hit.collider.name == "StickyNote1" || hit.collider.name == "StickyNote2") && EraseNoteSelectedbool)
                {
                    if (!chk4)
                    {
                        if (Temp_StickyNote_Refrence == null) // Other client is accessing data
                        {
                            Temp_StickyNote_Refrence = hit.collider.gameObject.transform;
                        }

                        if (Temp_StickyNote_Refrence != hit.collider.gameObject.transform)
                            Temp_StickyNote_Refrence = hit.collider.gameObject.transform;

                        if (hit.collider.gameObject.GetComponent<PhotonView>().Owner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
                        {
                      //     if(hit.collider.transform.childCount>0)
                         //       for(int i=0;i< hit.collider.transform.childCount;i++)
                           //         if(hit.collider.transform.GetChild(i).GetComponent<PhotonView>().Owner.ActorNumber != PhotonNetwork.LocalPlayer.ActorNumber)
                                   //     hit.collider.transform.GetChild(i).GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);


                            StartCoroutine(  GetALL_DataToRemove(hit.collider.gameObject));


                        }
                        else
                        {

                          
                            hit.collider.gameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer.ActorNumber);
                           

                            print(hit.collider.gameObject.transform.childCount + " Kids");                    
                            StartCoroutine(GetALL_DataToRemove(hit.collider.gameObject));
                      
                        }
                        //PhotonNetwork.Destroy(hit.collider.gameObject);
                        chk4 = true;
                        EraseNoteSelectedbool = false;
                    }

                }
                if (hit.collider.name == "Board" && !stickyBool  && !WriteNoteBool )
                {
                    if (chk1)
                    {
                        temptransform_localUse = PhotonNetwork.Instantiate(BoardName, hit.point, Quaternion.identity).transform;
                        ob.Push(temptransform_localUse.gameObject);
                        chk1 = false;
                    }

                    //
                    // Issue with stack . will resolve it later
                    if (BoardForTeacher)  // central Board as it has different axis
                        temptransform_localUse.position = new Vector3(OffsetBoard, hit.point.y, hit.point.z);     //   Instantiate(myPrefab, hit.point, Quaternion.identity);
                    else
                        temptransform_localUse.position = new Vector3(hit.point.x, hit.point.y, OffsetBoard);     //   Instantiate(myPrefab, hit.point, Quaternion.identity);
           //         if (!chk3)
                //        chk3 = true;
                }
                else if (hit.collider.name == "Board" && chk2) // spawn sticky check box
                {


                
                    print(BoardName);
                    if (BoardName == "Marker")
                    {
                        Temp_StickyNote_Refrence = PhotonNetwork.Instantiate("StickyNotes", hit.point, Quaternion.identity).transform;
                        Temp_StickyNote_Refrence.gameObject.name = "StickyNote";
                        Temp_StickyNote_Refrence.position = new Vector3(Temp_StickyNote_Refrence.position.x, Temp_StickyNote_Refrence.position.y, -15.167f);
                        Temp_StickyNote_Refrence.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
                        _Photonview.RPC("BroadCastNamesOfSpawnedNotes", RpcTarget.Others, Temp_StickyNote_Refrence.gameObject.GetComponent<PhotonView>().ViewID, Temp_StickyNote_Refrence.gameObject.name);
                    }
                    else if (BoardName == "Marker2")
                    {
                        Temp_StickyNote_Refrence = PhotonNetwork.Instantiate("StickyNotes2", hit.point, Quaternion.identity).transform;
                        Temp_StickyNote_Refrence.gameObject.name = "StickyNote2";
                        Temp_StickyNote_Refrence.position = new Vector3(25.807f, Temp_StickyNote_Refrence.position.y, Temp_StickyNote_Refrence.position.z);
                        Temp_StickyNote_Refrence.rotation = Quaternion.Euler(new Vector3(90, -90, 0));
                        _Photonview.RPC("BroadCastNamesOfSpawnedNotes", RpcTarget.Others, Temp_StickyNote_Refrence.gameObject.GetComponent<PhotonView>().ViewID, Temp_StickyNote_Refrence.gameObject.name);
                        print("sfsdf");
                    }
                    else if (BoardName == "Marker1")
                    {
                        Temp_StickyNote_Refrence = PhotonNetwork.Instantiate("StickyNotes1", hit.point, Quaternion.identity).transform;
                        Temp_StickyNote_Refrence.gameObject.name = "StickyNote1";
                        Temp_StickyNote_Refrence.position = new Vector3(Temp_StickyNote_Refrence.position.x, Temp_StickyNote_Refrence.position.y, OffsetBoard);
                        Temp_StickyNote_Refrence.rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
                        _Photonview.RPC("BroadCastNamesOfSpawnedNotes", RpcTarget.Others, Temp_StickyNote_Refrence.gameObject.GetComponent<PhotonView>().ViewID, Temp_StickyNote_Refrence.gameObject.name);

                    }// _Photonview.RPC("SetStickynoteTransform", RpcTarget.All, hit.point);
                    chk2 = false;
                    stickyBool = false;
                    StickyButton_WriteOnNote.gameObject.SetActive(true); 
                    StickyButton_MoveBoard.gameObject.SetActive(true);
                    StickyButton_EraseNote.gameObject.SetActive(true);
                    stickyButton_TypeOnNote.gameObject.SetActive(true);
                }
                if((hit.collider.name == "StickyNote" || hit.collider.name == "StickyNote1" || hit.collider.name == "StickyNote2") && WriteNoteBool)
                {
                    if (chk3)
                    {
                        if (Temp_StickyNote_Refrence != hit.collider.gameObject.transform)
                            Temp_StickyNote_Refrence = hit.collider.gameObject.transform;
                       if(hit.collider.name == "StickyNote")
                        tempTeansformForNoteMarker = PhotonNetwork.Instantiate("MarkerForNote", hit.point, Quaternion.identity).transform;
                        else if (hit.collider.name == "StickyNote1")
                            tempTeansformForNoteMarker = PhotonNetwork.Instantiate("MarkerForNote1", hit.point, Quaternion.identity).transform;
                        else if (hit.collider.name == "StickyNote2")
                            tempTeansformForNoteMarker = PhotonNetwork.Instantiate("MarkerForNote2", hit.point, Quaternion.identity).transform;
                        chk3 = false;
                    }
                    
                    if (BoardName == "Marker")
                        tempTeansformForNoteMarker.position = new Vector3(hit.point.x, hit.point.y,  OffsetBoard+0.03f);
                   else if (BoardName == "Marker1")
                        tempTeansformForNoteMarker.position = new Vector3(hit.point.x,  hit.point.y, OffsetBoard);
                   else if (BoardName == "Marker2")
                        tempTeansformForNoteMarker.position = new Vector3(OffsetBoard, hit.point.y, hit.point.z);
                    chk5 = false;

                }
                if (hit.collider.name == "Board" && !stickyBool && WriteNoteBool)  // Marker out of the box then in again
                {
                    if (WriteNoteBool)
                        if (tempTeansformForNoteMarker != null)
                        {
                            _Photonview.RPC("AddNoteMarkerTOChild", RpcTarget.All, tempTeansformForNoteMarker.gameObject.GetComponent<PhotonView>().ViewID, Temp_StickyNote_Refrence.gameObject.GetComponent<PhotonView>().ViewID);

                            chk3 = true;
                        }
                }
            }
        }
        else  /// its not me  
        {
            chk1 = true;
            if (WriteNoteBool)
                if (tempTeansformForNoteMarker != null && tempTeansformForNoteMarker.parent == null && chk5==false )
                {
                    //  Temp_StickyNote_Refrence.GetComponent<MakeChildTransformMove>().Object_to_Mesh(tempTeansformForNoteMarker.gameObject, myCamera); 
                    //  tempTeansformForNoteMarker.transform.parent = Temp_StickyNote_Refrence;
                           _Photonview.RPC("AddNoteMarkerTOChild", RpcTarget.All, tempTeansformForNoteMarker.gameObject.GetComponent<PhotonView>().ViewID, Temp_StickyNote_Refrence.gameObject.GetComponent<PhotonView>().ViewID);
                    //BakeLineDebuger(tempTeansformForNoteMarker.gameObject, myCamera);
                    chk5 = true;
                } chk3 = true;
        }
    }


    [PunRPC]
    void AddNoteMarkerTOChild(int Data, int parnt)
    {

        print("DataID"+ Data);
        print("rgrertert" + parnt);
       
        PhotonView _StickyboardLines; //   PhotonView.Find()
        _StickyboardLines = PhotonView.Find(Data);
   
         BakeLineDebuger(_StickyboardLines.gameObject,TempCameraBoard, PhotonView.Find(parnt).gameObject.transform);

           // _StickyboardLines.gameObject.transform.parent = PhotonView.Find(parnt).gameObject.transform;

        
    }
    public Material MyMesh;
   void  BakeLineDebuger(GameObject lineObj,Camera cam,Transform parent)
    {
        
          //  print("abcd");
            if (lineObj.GetComponent<TrailRenderer>() == null)
            {
                chk5 = false;

                return;
            }
            var Trail_Renderer = lineObj.GetComponent<TrailRenderer>();
            //   var meshFilter = lineObj.AddComponent<MeshFilter>();
            Mesh mesh = new Mesh();
          //  print("efgh");
            Trail_Renderer.BakeMesh(mesh, cam, true);
          //  print("ijkl");
           // yield return new WaitForSeconds(1.5f);
          //  print("monp");
            Trail_Renderer.GetComponent<MeshFilter>().sharedMesh = mesh;
          //  print("qrst");
            MeshRenderer meshRenderer = lineObj.AddComponent<MeshRenderer>();
         //   print("Uvwx");
        meshRenderer.material = MyMesh;              //.SetColor("_Color", Color.red);
            Destroy(Trail_Renderer);//.enabled = false;
            lineObj.transform.position = Vector3.zero;
         //   print("zee");
       lineObj.gameObject.transform.parent = parent;

        //   yield return null;
        //GameObject.Destroy(Trail_Renderer);
    }

}
